package tp3graph;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PlusCherOuMoinsCher extends JFrame implements ActionListener{
	private Container panneau;
	private JButton monBouton;
	private JTextField monChampTexte;
	private JLabel monLabel, monLabel2;
	private int devine = 1 + (int)(Math.random() * ((100 - 1) + 1));
	private int valeur;

	public PlusCherOuMoinsCher(){
		//Création de la fenêtre
		super("Mon Application Graphique");
		setSize(300, 150);
		setLocation(20,20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Récupération du container
		panneau = getContentPane();
		panneau.setLayout(new GridLayout(3,1));

		//Création des objets à utiliser
		monLabel = new JLabel("Votre proposition");
		monLabel2 = new JLabel("La réponse");
		monChampTexte = new JTextField("Saisir ici");
		monBouton = new JButton("Je vérifie");
		monBouton.addActionListener(this);

		//Ajout au panneau
		panneau.add(monLabel);
		panneau.add(monChampTexte);
		panneau.add(monBouton);
		panneau.add(monLabel2);

		setVisible(true);
	}
	public void afficher(){
		System.out.println(monChampTexte.getText());
	}
	
	public void comparer(){
		valeur = Integer.parseInt(monChampTexte.getText());
		if (valeur < devine){
			monLabel2.setText("Plus cher");
		}
		else if(valeur > devine){
			monLabel2.setText("Moins cher");
		}
		else{
			monLabel2.setText("Correct");
		}
		
	}

	public static void main(String[] args) {
		PlusCherOuMoinsCher n = new PlusCherOuMoinsCher();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		comparer();
		
		
	}

}
